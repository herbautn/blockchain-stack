{% from "map.jinja" import fabric_ca_image,fabric_orderer_image,fabric_peer_image,fabric_couchdb_image,fabric_tools_image with context %}

docker:
  registry-images:
    - {{ fabric_ca_image }}
    - {{ fabric_orderer_image }}
    - {{ fabric_peer_image }}
    - {{ fabric_couchdb_image }}
    - {{ fabric_tools_image }}
