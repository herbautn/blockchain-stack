{% from "map.jinja" import fabric_ca_image,fabric_orderer_image,fabric_peer_image,fabric_couchdb_image,fabric_tools_image with context %}

hl:
  channel_id: mychannel
  fabric_ca_image:  {{fabric_ca_image}}
  fabric_orderer_image: {{fabric_orderer_image}}
  fabric_peer_image:  {{fabric_peer_image}}
  fabric_couchdb_image:  {{fabric_couchdb_image}}
  fabric_tools_image:  {{fabric_tools_image}}
