


monitoring:
  - host: h0
placement:
  - orderer:
      host: h2
      port: 7050
      domain: nextnet.top
  - cryptogen:
    - host: h0
  - anchorPeers:
      org1: peer0.org1.nextnet.top
      org2: peer0.org2.nextnet.top

  - cas:
     - org1:
         host: h2
         port: 9558
         name: caorg1
     - org2:
         host: h2
         port: 9639
         name: caorg2

  - hostalias:
    - orderer.nextnet.top:
        host: h2

    - ca.org1.nextnet.top:
        host: h2
    - ca.org2.nextnet.top:
        host: h2

    - peer0.org1.nextnet.top:
        host: h1
        ports:
          rest: 7100
          peer: 8475
          cli: 8196
          event: 8263

    - peer1.org1.nextnet.top:
        host: h1
        ports:
          rest: 9566
          peer: 8962
          cli: 8109
          event: 8369

    - peer2.org1.nextnet.top:
        host: h1
        ports:
          rest: 7893
          peer: 8267
          cli: 9274
          event: 8069


    - caorg2.nextnet.top:
        host: h2

    - peer0.org2.nextnet.top:
        host: h1
        ports:
          rest: 9966
          peer: 9466
          cli: 9569
          event: 9484

    - peer1.org2.nextnet.top:
        host: h2
        ports:
          rest: 8840
          peer: 7931
          cli: 7011
          event: 9534

    - peer2.org2.nextnet.top:
        host: h2
        ports:
          rest: 9439
          peer: 8028
          cli: 9479
          event: 8504


OrdererOrgs:
  - Name: Orderer
    Domain: nextnet.top
    Specs:
      - Hostname: orderer

PeerOrgs:

  - Name: org1
    Domain: org1.nextnet.top
    CA:
      Hostname: ca
    Template:
      Count: 3
    Users:
      Count: 1

  - Name: org2
    Domain: org2.nextnet.top
    CA:
      Hostname: ca
    Template:
      Count: 3
    Users:
      Count: 1
