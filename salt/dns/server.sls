{% from "dns/map.jinja" import zones with context %}
{% from "dns/map.jinja" import hostaliases with context %}


dnsmasq:
  pkg.removed: []

bind9:
  pkg.installed: []
  service.running:
    - watch:
      - file: /etc/bind/named.conf.local
      - file: /etc/bind/named.conf.options
      {%- for zone in zones %}
      - file: /etc/bind/{{zone}}.db
      {%- endfor %}


/etc/bind/named.conf.local:
  file.managed:
    - source: salt://dns/etc/bind/named.conf.local
    - template: jinja


/etc/bind/named.conf.options:
  file.managed:
    - source: salt://dns/etc/bind/named.conf.options
    - template: jinja





{% for zone in zones %}
/etc/bind/{{zone}}.db:
  file.managed:
    - source: salt://dns/etc/bind/dns-zone.tpl
    - template: jinja
    - context:
        reccords:
          {% for hostalias in hostaliases -%} ###for all the aliases defined in the pillar
            {%- for alias,data in hostalias.items() -%}
              {%- if ".".join(alias.split(".")[1:]) == zone -%} #check if they belong to our zone
              {%- set a_name = alias.split(".")[0] %} #in such case, take the leftmost component
          {{a_name}}: {{salt["mine.get"]("*","controlpath_ip")[data["host"]][0] }} #and associate it with the ip
              {%- endif %}
            {% endfor -%}
          {%- endfor %}

{% endfor %}
