#install a DNS server and clients so that we can resolve components by name


install dns clients:
   salt.state:
     - tgt: "*"
     - sls:
       - dns.client

install dns server:
   salt.state:
     - tgt: "h0"
     - sls:
       - dns.server
