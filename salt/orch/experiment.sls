{% from "orch/map.jinja" import orderer_minion_id, cryptogen_minion_id with context %}


compute_crypto_material:
  salt.state:
    - tgt: {{ cryptogen_minion_id }}
    - sls:
      - hyperledger.02_cryptogen
      - hyperledger.03_artifacts


install orderer:
  salt.state:
    - tgt: {{orderer_minion_id}}
    - sls:
        - hyperledger.04_orderer

install peers:
  salt.state:
    - tgt: "*"
    - sls:
        - hyperledger.05_peer

install ca:
  salt.state:
    - tgt: "*"
    - sls:
        - hyperledger.06_ca
