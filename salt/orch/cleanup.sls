clean cache minion fs:
  salt.function:
    - tgt: '*'
    - name: cmd.run
    - arg:
      - rm -rf /var/cache/salt/master/minionfs/*

clean cache minion:
  salt.function:
    - tgt: '*'
    - name: cmd.run
    - arg:
      - rm -rf /var/cache/salt/master/minion/*

restart minions:
  salt.function:
    - tgt: "*"
    - name: cmd.run_bg
    - arg:
      - "salt-call --local service.restart salt-minion"

clean previous crypto:
  salt.function:
    - tgt: 'h0'
    - name: cmd.run
    - arg:
      - rm -rf /root/*

wipe running containers:
  salt.function:
    - tgt: '*'
    - name: cmd.run
    - arg:
      - docker rm -f $(docker ps -qa) || true
