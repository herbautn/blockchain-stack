#Here, we install hyperledger binaries and containers through a local registry to avoid
#excessive download time

dowload HL binaries:
  salt.state:
    - tgt: '*'
    - sls:
      - hyperledger.dl-hl-binaries
      - docker
      - docker.unsecure_registry


pull_docker_images:
  salt.state:
    - tgt: 'h0'
    - sls:
      - docker.load_registry

distribute_docker_mages:
  salt.state:
    - tgt: "*"
    - sls:
      - docker.pull_local_registry
