#-*- coding: utf-8 -*-
'''
:maintainer: Nicolas Herbaut
:maturity: 20150910
:requires: none
:platform: all
'''

import glob

def file_catch(pattern,*args, **kwargs):

  return [f.split("/")[-1] for f in glob.glob(pattern)]

