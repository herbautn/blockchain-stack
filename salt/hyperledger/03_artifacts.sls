{% from "hyperledger/map.jinja" import channel_id,orgs with context %}

/root/configtx.yaml:
  file.managed:
    - source: salt://hyperledger/configtx.yaml
    - template: jinja



/root/channel-artifacts:
  file.directory: []

#create the genesis block
/root/channel-artifacts/genesis.block:
  cmd.run:
    - name: configtxgen -profile TwoOrgsOrdererGenesis -outputBlock ./channel-artifacts/genesis.block
    - env:
      - FABRIC_CFG_PATH: /root




#Generating channel configuration transaction
/root/channel-artifacts/channel.tx:
  cmd.run:
    - name: configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID {{ channel_id }}
    - env:
      - FABRIC_CFG_PATH: /root




{% for org in  orgs %}
#Generating anchor peer update for {{org}}MSP
/root/channel-artifacts/{{ org["Name"] }}MSPanchors.tx:
  cmd.run:
    - name: configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/{{ org["Name"] }}MSPanchors.tx -channelID {{ channel_id }} -asOrg {{org["Name"]}}MSP
    - env:
      - FABRIC_CFG_PATH: /root


{% endfor %}


push /root/channel-artifacts/:
  module.run:
    - name: cp.push_dir
    - path: /root/channel-artifacts/
    - fire_event: salt/{{grains.id}}/cp/push/finished
    - watch:
      - cmd: /root/channel-artifacts/channel.tx
      {% for org in  orgs %}
      - cmd: /root/channel-artifacts/{{ org["Name"] }}MSPanchors.tx
      {% endfor %}
      - cmd: /root/channel-artifacts/genesis.block
