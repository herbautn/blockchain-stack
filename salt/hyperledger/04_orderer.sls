{% from "hyperledger/map.jinja" import fabric-orderer-image,dns_server,orderer,hl_version,crypto_minion with context %}

orderer:
  docker_container.running:
    - network_mode: bridge
    - dns: {{dns_server}}
    - image: {{fabric_orderer_image}}
    - environment:
      - ORDERER_GENERAL_LOGLEVEL: debug
      - ORDERER_GENERAL_LISTENADDRESS: 0.0.0.0
      - ORDERER_GENERAL_LISTENPORT: {{orderer["port"]}}
      - ORDERER_GENERAL_GENESISMETHOD: file
      - ORDERER_GENERAL_GENESISFILE: /var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID: OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR: /var/hyperledger/orderer/msp
      - ORDERER_GENERAL_TLS_ENABLED: true
      - ORDERER_GENERAL_TLS_PRIVATEKEY: /var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE: /var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS: "[/var/hyperledger/orderer/tls/ca.crt]"
    - working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    #- command: bash -c "orderer"
    - command: sleep infinity
    - binds:
       - /root/channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block:ro
       - /root/crypto-config/ordererOrganizations/nextnet.top/orderers/orderer.nextnet.top/msp:/var/hyperledger/orderer/msp:ro
       - /root/crypto-config/ordererOrganizations/nextnet.top/orderers/orderer.nextnet.top/tls:/var/hyperledger/orderer/tls:ro
    - port_bindings:
      - {{orderer["port"]}}:{{orderer["port"]}}
