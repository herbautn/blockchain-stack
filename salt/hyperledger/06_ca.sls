{% from "hyperledger/map.jinja" import orgs,hl_version,fabric-ca-image,dns_server with context %}


{% for org in orgs %}
  {% set ca=salt["pillar.get"]("placement:cas:%s"%org['Name']) %}
  {% set keyfile=salt["spy.file_catch"]("/root/crypto-config/peerOrganizations/%s/tlsca/*_sk"%org["Domain"])[0] %}
{% if ca["host"]==grains.id %}
{{ ca["name"] }}:
  docker_container.running:
    - network_mode: bridge
    - dns: {{dns_server}}
    - image: {{fabric_ca_image}}
    - environment:
      - FABRIC_CA_HOME: /etc/hyperledger/fabric-ca-server
      - FABRIC_CA_SERVER_CA_NAME: ca-{{org["Name"]|lower}}
      - FABRIC_CA_SERVER_TLS_ENABLED: false
      - FABRIC_CA_SERVER_TLS_CERTFILE: /etc/hyperledger/fabric-ca-server-config/tlsca.{{org["Domain"]}}-cert.pem
      - FABRIC_CA_SERVER_TLS_KEYFILE: /etc/hyperledger/fabric-ca-server-config/{{keyfile}}
    - port_bindings:
      - {{ca["port"]}}:7054
    #- command: sh -c 'fabric-ca-server -d start --ca.certfile /etc/hyperledger/fabric-ca-server-config/tlsca.{{org["Domain"]}}-cert.pem --ca.keyfile /etc/hyperledger/fabric-ca-server-config/{{keyfile}} -b admin:adminpw -d'
    - command: sleep infinity
    - binds:
      - /root/crypto-config/peerOrganizations/{{org["Domain"]}}/tlsca/:/etc/hyperledger/fabric-ca-server-config


{% endif %}
{% endfor %}

echo "Ca installed":
  cmd.run
