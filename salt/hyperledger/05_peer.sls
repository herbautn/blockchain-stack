{% from "hyperledger/map.jinja" import orgs, fabric_peer_image,anchorPeers,dns_server,orderer,hl_version,crypto_minion with context %}



/root/peer.txt:
  file.managed:
    - template: jinja
    - source: salt://hyperledger/peer.tpl


{% for org in orgs %}


  {% set anchorPeer=anchorPeers[org["Name"]] %}

    {% for peer in range( org['Template']['Count'] ) %}
      {% set name="peer%d.%s"%(peer,org['Domain']) %}
      {% set phy_host=salt["pillar.get"]("placement:hostalias:%s:host"%name) %}
      {% if phy_host==grains.id%}
      {% set phy_host_ip=salt["mine.get"]("*",'controlpath_ip')[phy_host][0] %}
      {% set phy_port_rest=salt["pillar.get"]("placement:hostalias:%s:ports:rest"%name) %}
      {% set phy_port_peer=salt["pillar.get"]("placement:hostalias:%s:ports:peer"%name) %}
      {% set phy_port_cli=salt["pillar.get"]("placement:hostalias:%s:ports:cli"%name) %}
      {% set phy_port_event=salt["pillar.get"]("placement:hostalias:%s:ports:event"%name) %}

{{ name }}:
    docker_container.running:
      - image: {{fabric_peer_image}}
      - dns: {{ salt["mine.get"]("*","controlpath_ip")[salt["pillar.get"]("bind:config:dns-server:host")][0] }}
      - name: p{{peer}}{{org["Name"]}}
      - environment:
        - CORE_LOGGING_CAUTHDSL=DEBUG
        - CORE_LOGGING_GOSSIP=DEBUG
        - CORE_LOGGING_LEDGER=DEBUG
        - CORE_LOGGING_MSP=DEBUG
        - CORE_LOGGING_POLICIES=INFO
        - CORE_LOGGING_GRPC=INFO
        - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
        - CORE_LOGGING_LEVEL=DEBUG
        - CORE_PEER_TLS_ENABLED=true
        - CORE_PEER_GOSSIP_USELEADERELECTION=true
        - CORE_PEER_PROFILE_ENABLED=true
        - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
        - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
        - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
        - CORE_PEER_ID={{name}}
        - CORE_PEER_CHAINCODELISTENADDRESS=127.0.0.1:{{phy_port_cli}}
        - CORE_PEER_ADDRESS={{name}}:{{phy_port_peer}}
        - CORE_PEER_LISTENADDRESS=0.0.0.0:{{phy_port_peer}}
        - CORE_PEER_GOSSIP_ORGLEADER=false
        {% if anchorPeer["id"]!=name -%}
        - CORE_PEER_GOSSIP_EXTERNALENDPOINT={{name}}:{{phy_port_peer}}
        - CORE_PEER_GOSSIP_BOOTSTRAP={{anchorPeer["id"]}}:{{anchorPeer["port"]}}
        {%- else %}
        - CORE_PEER_GOSSIP_BOOTSTRAP=127.0.0.1:{{phy_port_peer}}
        {%- endif %}
        - CORE_PEER_LOCALMSPID={{org["Name"]}}MSP
      #- command: bash -c "sleep 3 && peer node start"
      - command: "sleep infinity"
      - binds:
        - /var/run/:/host/var/run/
        - /root/crypto-config/peerOrganizations/{{org["Domain"]}}/peers/{{name}}/msp:/etc/hyperledger/fabric/msp
        - /root/crypto-config/peerOrganizations/{{org["Domain"]}}/peers/{{name}}/tls:/etc/hyperledger/fabric/tls
      - port_bindings:
          - {{phy_port_rest}}:{{phy_port_rest}} #7050
          - {{phy_port_peer}}:{{phy_port_peer}} #7051
          - {{phy_port_cli}}:{{phy_port_cli}} #7052
          - {{phy_port_event}}:{{phy_port_event}} #7053


          {% endif %}

    {% endfor %}
{% endfor %}
