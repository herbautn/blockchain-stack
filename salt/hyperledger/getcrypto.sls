{% from "hyperledger/map.jinja" import crypto_minion with context %}

Get /root/crypto-config/ from {{crypto_minion}}:
  module.run:
    - name: cp.get_dir
    - path: salt://{{crypto_minion}}/root/crypto-config
    - dest: /root

Get /root/channel-artifacts/ from {{crypto_minion}}:
  module.run:
    - name: cp.get_dir
    - path: salt://{{crypto_minion}}/root/channel-artifacts
    - dest: /root
