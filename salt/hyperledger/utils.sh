
salt "*" state.apply hyperledger
salt "*" state.apply hyperledger.01_docker
salt "*" pkg.install python-pip
salt "*" pip.install docker
salt "*" state.apply docker_spy


rm -rf /var/cache/salt/master/minionfs/*
rm -rf /var/cache/salt/master/minions/*

salt "*" cmd.run 'docker rm -f $(docker ps -qa)'
salt "*" cmd.run 'rm -rf /root/*'
salt "*" saltutil.sync_all
salt "*" state.apply dns.client
salt "h0" state.apply dns.server


salt "*" state.apply hyperledger.dl-hl-binaries
salt "*" state.apply hyperledger.02_cryptogen
salt "*" state.apply hyperledger.03_artifacts
salt "*" state.apply hyperledger.04_orderer
salt "*" state.apply hyperledger.05_peer
salt "*" state.apply hyperledger.06_ca
salt "*" state.apply hyperledger.07_script
salt "*" state.apply hyperledger.08_cli
salt "*" state.apply hyperledger.utils
#echo docker exec  cli bash -c "/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/script.sh mychannel 10"

#echo docker exec  cli bash -c "CORE_PEER_TLS_ENABLED=false /opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/script.sh mychannel 10"
salt "*" mine.update
salt "*" state.apply tickstack,nftables,telegraf
docker exec -ti cli bash

{# {% set org = (salt["pillar.get"]("placement:anchorPeers").keys()|list)[0] %} #}
{% set org = "org1" %}
{% set hostname = salt["pillar.get"]("placement:anchorPeers:"+org) %}
{% set peer_port = salt["pillar.get"]("placement:hostalias:"+hostname+":ports:peer") %}
export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/{{org}}.nextnet.top/users/Admin@{{org}}.nextnet.top/msp
export CORE_PEER_ADDRESS={{hostname}}:{{peer_port}}
export CORE_PEER_LOCALMSPID="{{org}}MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/{{org}}.nextnet.top/peers/{{hostname}}/tls/ca.crt
export CHANNEL_NAME=mychannel
export CORE_PEER_TLS_ENABLED=false

peer channel create -o orderer.nextnet.top:{{salt["pillar.get"]("placement:orderer:port")}} -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/nextnet.top/orderers/orderer.nextnet.top/msp/tlscacerts/tlsca.nextnet.top-cert.pem
peer channel join -b ./mychannel.block
peer chaincode install -n mycc -v 1.0 -p github.com/hyperledger/fabric/examples/chaincode/go/chaincode_example02
peer chaincode instantiate -o orderer.nextnet.top:{{salt["pillar.get"]("placement:orderer:port")}} --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C $CHANNEL_NAME -n mycc -v 1.0 -c '{"Args":["init","a", "100", "b","200"]}' -P "OR ('{{org}}MSP.member')"
