/root/crypto-config.yaml:
  file.managed:
    - template: jinja
    - source: salt://crypto-config.sls

/root/crypto-config:
  cmd.run:
    - name: cryptogen generate --config /root/crypto-config.yaml
    - cwd: /root/
    - require:
      - file: /root/crypto-config.yaml


push /root/crypto-config:
  module.run:
    - name: cp.push_dir
    - path: /root/crypto-config
    - watch:
      - cmd: /root/crypto-config
