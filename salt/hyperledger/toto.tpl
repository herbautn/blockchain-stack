include:
  - golang
  - docker.compose

git:
  pkg.installed: []

hyperledger-fabric-samples:
  git.latest:
    - name: https://github.com/hyperledger/fabric-samples.git
    - branch: master
    - target: /opt/hyperledger/fabric-samples


hyperledger-fabric-binaries:
  cmd.run:
    - name: curl -sSL https://goo.gl/byy2Qj | bash -s {{ salt["pillar.get"]('hl:version') }}
    - cwd: /usr
