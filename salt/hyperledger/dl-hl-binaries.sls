hyperledger-fabric-samples:
  git.latest:
    - name: https://github.com/hyperledger/fabric-samples.git
    - branch: master
    - target: /opt/hyperledger/fabric-samples
    - force_clone: True


hyperledger-fabric-binaries:
  cmd.script:
    - source: salt://hyperledger/download-hl-binaries.sh
    - cwd: /usr/local/

push /usr/local/bin:
  module.run:
    - name: cp.push_dir
    - path: /usr/local/bin
    - watch:
      - cmd: hyperledger-fabric-binaries

