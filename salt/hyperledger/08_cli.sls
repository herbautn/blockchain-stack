{% from "hyperledger/map.jinja" import hl_version,dns_server,fabric_tools_image with context %}



cli:
  docker_container.running:
    - network_mode: bridge
    - dns: {{ dns_server }}
    - image: {{fabric_tools_image}}
    - tty: true
    - environment:
      - GOPATH: /opt/gopath
      - CORE_VM_ENDPOINT: unix:///host/var/run/docker.sock
      - CORE_LOGGING_LEVEL: DEBUG
      - CORE_PEER_ID: cli
      - CORE_PEER_ADDRESS: peer0.org1.example.com:7051
      - CORE_PEER_LOCALMSPID: Org1MSP
      - CORE_PEER_TLS_ENABLED: true
      - CORE_PEER_TLS_CERT_FILE: /etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE: /etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE: /etc/hyperledger/fabric/tls/ca.crt
      - CORE_PEER_MSPCONFIGPATH: /etc/hyperledger/fabric/msp
    - working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    #- command: /bin/bash -c './scripts/script.sh ${CHANNEL_NAME} ${DELAY}; sleep $TIMEOUT'
    - command: sleep infinity
    - binds:
        - /root/channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
        - /root/crypto-config/ordererOrganizations/nextnet.top/orderers/orderer.nextnet.top/msp:/var/hyperledger/orderer/msp:ro
        - /root/crypto-config/ordererOrganizations/nextnet.top/orderers/orderer.nextnet.top/tls:/var/hyperledger/orderer/tls:ro
        - /root/crypto-config/peerOrganizations/org1.nextnet.top/:/var/hyperledger/peer/:ro
